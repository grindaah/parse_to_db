#coding: utf-8

#brief: parse poor text file into good old db-tables

import sqlite3

sqlite_file = 'dempfers_db.sqlite'                         # name of the sqlite database file
table_name1 = 'my_table_1'                                 # name of the table to be created
new_field = 'DName'                                # name of the column
field_type = 'CHAR'                                     # column data type
new_field2 = 'K1_H'                               # name of the column
field_type2 = 'INTEGER'
new_field3 = 'K2_H'
field_type3 = 'INTEGER'
new_field4 = 'C1_H'
field_type4 = 'INTEGER'
new_field5 = 'C2_H'
field_type5 = 'INTEGER'

# Connecting to the database file
conn = sqlite3.connect(sqlite_file)
c = conn.cursor()

# Creating a new SQLite table with 1 column
#c.execute('CREATE TABLE {tn} ({nf} {ft} {nf2} {ft2} {nf3} {ft3} {nf4} {ft4} {nf5} {ft5})'\
#                .format(tn=table_name1, nf=new_field, ft=field_type, nf2=new_field2, ft2=field_type2
#                    , nf3=new_field3, ft3=field_type3, nf4=new_field4, ft4=field_type4, nf5=new_field5, ft5=field_type5))

# Committing changes and closing the connection to the database file
#conn.commit()

sql_insert = "INSERT INTO {tn} ({nf} {nf2} {nf3} {nf4} {nf5}) VALUES ('DBH-7200', 7200, 2400, 1200, 599)".format(tn=table_name1,
        nf=new_field, nf2=new_field2, nf3=new_field3, nf4=new_field4, nf5=new_field5)

#try:
c.execute(sql_insert)
conn.commit()
#except:
#    print "not acha"
#    conn.rollback()

conn.close()
