#coding: utf-8

import os
import sqlite3
from sys import argv

sqlite_file = 'dempfers_db.sqlite'

class Damper:
    def __init__(self, name, k1_h, k2_h, c1_h, c2_h, k1_v = 0.0, k2_v = 0.0, c1_v = 0.0, c2_v = 0.0):
        self.name = name
        self.k1_h = k1_h
        self.k2_h = k2_h
        self.c1_h = c1_h
        self.c2_h = c2_h

    def __str__(self):
        return "%s: K1=%.2f K2=%.2f C1=%.2f C2=%.2f" % (self.name, self.k1_h, self.k2_h, self.c1_h, self.c2_h)

    def __repr__(self):
        return str(self)

def main(options):
    show()
    #default db filename
    filename = "dmp.dbs"
    dampers = []

    print dampers
    if options:
        filename = options[0]

    lines = [line for line in open(filename) if line.lower().count("name") == 0]
    for line in lines:
        print "processing line: %s" % line
        damper_data = line.split()
        dampers.append(Damper(damper_data[0],
            float(damper_data[1]),
            float(damper_data[2]),
            float(damper_data[3]),
            float(damper_data[4])))

    store_to_sql(dampers)
    #print "%s" % (dampers)
        
#    can i use generator here?
#    dampers = [Damper(line.split()[0] )]


def store_to_sql(dampers):
    table_name = 'my_table_1'
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    for damper in dampers:
        sql_insert = "INSERT INTO {tn} VALUES ('{name}', {fd1}, {fd2}, {fd3}, {fd4})".format(tn=table_name, name=damper.name, fd1=damper.k1_h, fd2=damper.k2_h, fd3=damper.c1_h, fd4=damper.c2_h)
        #try:
        c.execute(sql_insert)
        conn.commit()
        #except:
        print sql_insert + " failed"
        #    conn.rollback()

    conn.close()

def show():
    table_name = 'my_table_1'
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    query = "SELECT * FROM %s" % table_name
    c.execute(query)
    rows = c.fetchall()
    for row in rows: print row

def store_single_damper(line):
    pass


if __name__ == "__main__":
    main(argv[1:])
